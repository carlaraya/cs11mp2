/*
 * Carl Angelo Araya
 * 201508809
 * THXY
 */

#include <ctype.h>
#include <float.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>

#define DATAFILE "songs.dat"
#define LINESPERITEM 9

#define TITLE 0
#define ARTIST 1
#define COMPOSER 2
#define ALBUM 3
#define REMARKS 4
#define NUMP 5 // number of properties (the ones listed above)

#define ART 0
#define POP 1
#define TRADITIONAL 2
#define NUMG 3 // number of genres

/*
 * Struct with a name of Song. Contains an id, rating, genre, and an array of
 * char ptrs for each property not mentioned.
 */
typedef struct Song
{
    unsigned int id;
    char *p[NUMP]; // array of property strings
    double rating;
    int genre;

    struct Song *next;
} Song;

/*******************
 * Major functions *
 *******************/
/*
 * Load datafile and put all data in one linked list. Return linked list.
 */
Song *load_data(void);

/*
 * Ask for song data to be added to song node, then add it to linked list.
 */
void add_song(Song **headAddr);

/*
 * Ask for song to update, then get new song information and apply update.
 */
void update_song(Song **headAddr);

/*
 * Ask for song to delete, then remove song from linked list.
 */
void delete_song(Song **headAddr);

/*
 * Ask which field and keyword the user would want to search, then print all
 * songs that match the criteria.
 */
void list_songs(Song *head);

/*
 * Write back linked list to data file, and free linked list and contents
 * simultaneously.
 */
void clean_up(Song *head);

/*************************
 * Linked list functions *
 *************************/
/*
 * Ask for song data which will be added to the s parameter.
 */
void ask_for_data(Song *s);

/*
 * Add an existing node to its appropriate position.
 */
void add_song_node(Song **headAddr, Song *newSong);

/*
 * Ask user to enter title of a song, then if more than one song is found, ask
 * for the unique ID. Return the pointer if song is found, NULL if otherwise.
 */
Song *query_one_node(Song *head, Song **prevAddr);

/*
 * Print node contents. If itemNo is not 0, print item number. If is_printid
 * is true, print id.
 */
void print_node(Song *s, int itemNo, int is_printid);

/*
 * Free a node's contents, then the node itself.
 */
void free_node(Song *s);

/*
 * Free all nodes in a list.
 */
void free_list(Song *head);

void sort_list(Song **headAddr, int choice);

/********************
 * Helper functions *
 ********************/
/*
 * Get input string up until newline. and store it dynamically. Must free
 * output to avoid memory leak. Return input string.
 */
char *f_get_string(FILE *stream);

/*
 * Get input integer. Return input integer if valid input, otherwise return
 * INT_MAX.
 */
int f_get_int(FILE *stream);

/*
 * Get input double. Return input double if valid input, otherwise return
 * DBL_MAX.
 */
double f_get_double(FILE *stream);

/*
 * Get input hex integer. Return input integer if valid input, otherwise
 * return INT_MAX.
 */
int f_get_hex(FILE *stream);

/*
 * Use rand() to make the ID random. If new number is not unique, try again.
 */
void gen_id(Song *head, Song *s);
/*
 * Remove leading and trailing spaces.
 */
void remove_edge_spaces(char *s);

/*
 * Compare two songs based on title and artist. Return <0 if s1 comes before
 * s2, >0 if s1 comes after s2, and 0 if s1 and s2 have the same title & artist
 */
int songcmp(Song *s1, Song *s2);

/*
 * Return 1 if a is substring of b. Otherwise, return 0.
 */
int a_substring_of_b(const char *a, const char *b);

/*
 * The name says it all. Wait for user to press enter.
 */
void wait_enter(void);

int main(void)
{
    printf("Loading data...\n");
    Song *head = load_data();
    printf("Done! (press enter to continue)\n");
    wait_enter();

    int input;
    int isquit = 0;
    printf("Music Record Library v1.0\n\n");
    do
    {
        printf("1: Add Song\n");
        printf("2: Update Song\n");
        printf("3: Delete Song\n");
        printf("4: List Songs\n");
        printf("5: Exit Library\n");
        printf("6: Exit without saving changes\n");
        printf("Enter option number: ");
        input = f_get_int(stdin);

        switch(input)
        {
            case 1:
                add_song(&head);
                break;
            case 2:
                update_song(&head);
                break;
            case 3:
                delete_song(&head);
                break;
            case 4:
                list_songs(head);
                break;
            case 5:
                printf("Saving data... do not terminate to avoid losing data...\n");
                clean_up(head);
                printf("Done saving data.\n");
                isquit = 1;
                break;
            case 6:
                free_list(head);
                isquit = 1;
                break;
            default:
                printf("Invalid input.\n\n");
        }
    } while (!isquit);

    printf("Goodbye!\n");
    return 0;
}

/*******************
 * Major functions *
 *******************/
/*
 * Load datafile and put all data in one linked list. Return linked list.
 */
Song *load_data(void)
{
    srand(time(NULL)); // Used for generating IDs
    FILE *df = fopen(DATAFILE, "r");
    if (df == NULL)
    {
        return NULL;
    }

    Song *head = NULL;
    Song *ptr = NULL;
    Song *newSong = NULL;

    int numSongs = 0;
    char ch;
    while ((ch = fgetc(df)) != '\n' && !feof(df))
    {
        ungetc(ch, df);

        // Create new Song node, extract data and add data to node
        newSong = malloc(sizeof(Song));
        newSong->next = NULL;

        newSong->id = f_get_hex(df);
        int i;
        newSong->p[0] = f_get_string(df);
        for (i = 1; i < NUMP; i++)
        {
            newSong->p[i] = f_get_string(df);
        }
        newSong->rating = f_get_double(df);
        newSong->genre = f_get_int(df);
        char *hyphens = f_get_string(df);

        /*
         * Fail-safe mechanisms
         *  - Just in case someone would try and edit the datafile.
         */
        // Check if all data are valid
        int id_valid = newSong->id != INT_MAX;
        int title_valid = strlen(newSong->p[0]) != 0;
        int rating_valid = newSong->rating != DBL_MAX;
        int genre_valid = newSong->genre != INT_MAX;
        int hyphens_valid = strcmp("----------", hyphens) == 0;
        int all_valid = id_valid && title_valid && rating_valid
            && genre_valid && hyphens_valid;
        if (!all_valid)
        {
            printf("Error: incorrect datafile syntax\n");
            printf("-----> line ");
            if (!id_valid)
                printf("%d: invalid ID\n",
                        numSongs * LINESPERITEM + 1);
            else if (!title_valid)
                printf("%d: invalid title\n",
                        numSongs * LINESPERITEM + 2);
            else if (!rating_valid)
                printf("%d: invalid rating\n",
                        numSongs * LINESPERITEM + 7);
            else if (!genre_valid)
                printf("%d: invalid genre\n",
                        numSongs * LINESPERITEM + 8);
            else if (!hyphens_valid)
                printf("%d: invalid hyphens\n",
                        numSongs * LINESPERITEM + 9);
            printf("Recommended action: ");
            printf("exit without saving changes, then edit data.\n");
            wait_enter();
            free(hyphens);
            break;
        }
        // Check if newSong should be put to the end of the list. If not, use
        // add_song_node to put it in the correct (sorted) place.
        if (head == NULL)
        {
            head = newSong;
            ptr = head;
        }
        else
        {
            if (songcmp(ptr, newSong) < 0)
            {
                ptr->next = newSong;
                ptr = ptr->next;
            }
            else
            {
                add_song_node(&head, newSong);
            }
        }
        // Check if there are any identical IDs. If found, replace ID then
        // notify user about it.
        ptr = head;
        while (1)
        {
            if (ptr != newSong && ptr->id == newSong->id)
            {
                gen_id(head, newSong);
                printf("WARNING: ID conflict detected.\n");
                printf("Changed item %d id to %08x\n", numSongs+1, newSong->id);
                break;
            }
            if (ptr->next == NULL)
                break;
            ptr = ptr->next;
        }

        // Move ptr to end of list
        while (ptr->next != NULL)
            ptr = ptr->next;


        free(hyphens);
        numSongs++;
    }

    printf("%d items loaded successfully.\n", numSongs);
    fclose(df);
    return head;
}

/*
 * Ask for song data to be added to song node, then add it to linked list.
 */
void add_song(Song **headAddr)
{
    Song *newSong = malloc(sizeof(Song));
    ask_for_data(newSong);
    add_song_node(headAddr, newSong);
    gen_id(*headAddr, newSong);
    printf("Song added successfully. (ID: %08x)\n\n", newSong->id);
}

/*
 * Ask for song to update, then get new song information and apply update.
 */
void update_song(Song **headAddr)
{
    Song *head = *headAddr;
    Song *prev;
    Song *s = query_one_node(head, &prev);
    if (s == NULL)
    {
        printf("\n");
        return;
    }

    printf("\nSONG TO BE UPDATED:\n");
    printf("--------------------\n");
    print_node(s, 0, 1);

    printf("Press enter to start typing in new fields.\n");
    printf("Or type a to abort: ");
    char *input = f_get_string(stdin);

    if (strcmp(input, "a") == 0)
    {
        free(input);
        printf("\n");
        return;
    }

    // Free all contents before updating
    free(input);
    int i;
    for (i = 0; i < NUMP; i++)
        free(s->p[i]);

    ask_for_data(s);
    printf("\nDone updating.\n");

    if (prev == NULL)
        *headAddr = s->next;
    else
        prev->next = s->next;
    add_song_node(headAddr, s);
    printf("\n");
}

/*
 * Ask for song to delete, then remove song from linked list.
 */
void delete_song(Song **headAddr)
{
    Song *head = *headAddr;
    Song *prev;
    Song *s = query_one_node(head, &prev);
    if (s == NULL)
    {
        printf("\n");
        return;
    }

    printf("\nSONG TO BE DELETED:\n");
    printf("--------------------\n");
    print_node(s, 0, 1);

    printf("Confirm delete? (y/n):  \n");
    char *input = f_get_string(stdin);
    if (strcmp(input, "y") == 0)
    {
        if (prev == NULL)
            *headAddr = s->next;
        else
            prev->next = s->next;
        free_node(s);
        printf("Song deleted.\n");
    }
    else
        printf("Song delete cancelled.\n");
    printf("\n");
    free(input);
}

/*
 * Ask which field and keyword the user would want to search, then print all
 * songs that match the criteria.
 */
void list_songs(Song *head)
{
    printf("1: Title\n");
    printf("2: Artist\n");
    printf("3: Composer\n");
    printf("4: Album\n");
    printf("5: Rating\n");
    printf("6: Genre\n");
    printf("7: All\n");
    printf("What do you want to search?\n");
    int input;
    while (1)
    {
        printf("Type number of choice: ");
        input = f_get_int(stdin);
        if (input == INT_MAX || input < 1 || input > 7)
            printf("Invalid input.\n");
        else
            break;
    }
    int ct = 0;
    Song *ptr = head;

    if (input >= 1 && input <= 4)
    {
        char *query;
        int field = input - 1;
        while (1)
        {
            printf("Enter keyword to search: ");
            query = f_get_string(stdin);
            if (strlen(query) == 0)
                printf("Keyword must not be empty.\n");
            else
                break;
        }

        printf("\n--------------------\n");
        while (ptr != NULL)
        {
            if (a_substring_of_b(query, ptr->p[field]))
            {
                print_node(ptr, ++ct, 0);
            }
            ptr = ptr->next;
        }
        free(query);
    }
    else if (input == 5)
    {
        double queryMin;
        double queryMax;
        while (1)
        {
            printf("Enter minimum rating to search: ");
            queryMin = f_get_double(stdin);
            if (queryMin < 1 || queryMin > 5)
                printf("Input should be between 1 and 5, inclusive.\n");
            else
                break;
        }
        while (1)
        {
            printf("Enter maximum rating to search: ");
            queryMax = f_get_double(stdin);
            if (queryMax < 1 || queryMax > 5)
                printf("Input should be between 1 and 5, inclusive.\n");
            else if (queryMax < queryMin)
                printf("Input should be greater than minimum rating\n");
            else
                break;
        }

        sort_list(&head, 1);
        ptr = head;
        printf("\n--------------------\n");
        while (ptr != NULL)
        {
            if (ptr->rating >= queryMin && ptr->rating <= queryMax)
            {
                print_node(ptr, ++ct, 0);
            }
            ptr = ptr->next;
        }
        sort_list(&head, 2);

    }
    else if (input == 6)
    {
        int query;
        while (1)
        {
            printf("Enter genre to search:\n");
            printf("1 for art, 2 for pop, 3 for traditional\n");
            query = f_get_int(stdin);
            if (query < 1 || query > 3)
                printf("Invalid input.\n");
            else
                break;
        }
        query--;

        printf("\n--------------------\n");
        while (ptr != NULL)
        {
            if (ptr->genre == query)
            {
                print_node(ptr, ++ct, 0);
            }
            ptr = ptr->next;
        }
    }
    else if (input == 7)
    {
        printf("\n--------------------\n");
        while (ptr != NULL)
        {
            print_node(ptr, ++ct, 1);
            ptr = ptr->next;
        }
    }
    printf("Total Items: %d\n", ct);
    if (ct > 0)
        printf("Shift+PgUp/PgDn or Ctrl+Shift+Up/Down to scroll up and down.\n");
    printf("\n");
}

/*
 * Write back linked list to data file, and free linked list and contents
 * simultaneously.
 */
void clean_up(Song *head)
{
    FILE *db = fopen(DATAFILE, "w");
    Song *ptr = head;
    while (ptr != NULL)
    {
        head = ptr;
        ptr = ptr->next;

        fprintf(db, "%08x\n", head->id);
        int i;
        for (i = 0; i < NUMP; i++)
        {
            fprintf(db, "%s\n", head->p[i]);
        }
        fprintf(db, "%lf\n", head->rating);
        fprintf(db, "%d\n", head->genre);
        fprintf(db, "----------\n");
        free_node(head);
    }
    fclose(db);
}

/*************************
 * Linked list functions *
 *************************/
/*
 * Ask for song data which will be added to the s parameter.
 */
void ask_for_data(Song *s)
{
    char *properties[] =
                {"title", "artist", "composer", "album", "remarks"};
    int i;
    while (1)
    {
        printf("Enter song %s: ", properties[TITLE]);
        s->p[TITLE] = f_get_string(stdin);
        remove_edge_spaces(s->p[TITLE]);
        if (strlen(s->p[TITLE]) == 0)
            printf("This field cannot be left blank.\n");
        else
            break;
    }
    for (i = 1; i < NUMP; i++)
    {
        printf("Enter song %s: ", properties[i]);
        s->p[i] = f_get_string(stdin);
        remove_edge_spaces(s->p[i]);
    }
    while (1)
    {
        printf("Enter song rating: ");
        s->rating = f_get_double(stdin);
        if (s->rating > 5 || s->rating < 1)
            printf("Rating must be a decimal between 1 and 5\n");
        else
            break;
    }
    while (1)
    {
        printf("Enter song genre (1 for art, 2 for pop, 3 for traditional): ");
        s->genre = f_get_int(stdin) - 1;
        if (s->genre >= NUMG || s->genre < 0)
            printf("Genre must be an integer between 1 and %d\n", NUMG);
        else
            break;
    }
}

/*
 * Add an existing node to its appropriate position.
 */
void add_song_node(Song **headAddr, Song *newSong)
{
    if (*headAddr == NULL)
    {
        (*headAddr) = newSong;
        newSong->next = NULL;
        return;
    }
    Song *curr = *headAddr;
    Song *prev = NULL;
    while (curr != NULL)
    {
        if (songcmp(curr, newSong) >= 0)
        {
            if (prev == NULL)
            {
                *headAddr = newSong;
            }
            else
            {
                prev->next = newSong;
            }
            newSong->next = curr;
            
            return;
        }
        prev = curr;
        curr = curr->next;
    }
    prev->next = newSong;
    newSong->next = NULL;
}

/*
 * Ask user to enter title of a song, then if more than one song is found, ask
 * for the unique ID. Return the pointer if song is found, NULL if otherwise.
 */
Song *query_one_node(Song *head, Song **prevAddr)
{
    char *input;
    while (1)
    {
        printf("Enter keyword for title to search: ");
        input = f_get_string(stdin);
        if (strlen(input) == 0)
            printf("Keyword must not be empty.\n");
        else
            break;
    }
    char query[strlen(input) + 1];
    strcpy(query, input);
    free(input);

    int ct = 0;
    Song *ptr = head;
    printf("\n--------------------\n");
    while (ptr != NULL)
    {
        if (a_substring_of_b(query, ptr->p[TITLE]))
        {
            print_node(ptr, ++ct, 1);
        }
        ptr = ptr->next;
    }
    printf("Total Items: %d\n", ct);
    if (ct == 0)
    {
        printf("Nothing matched the query.\n\n");
        return NULL;
    }
    printf("Shift+PgUp/PgDn or Ctrl+Shift+Up/Down to scroll up and down.\n");
    int id;
    if (ct > 1)
    {
        while (1)
        {
            printf("Type ID of song to search: ");
            id = f_get_hex(stdin);
            if (id == INT_MAX)
                printf("Invalid input.\n");
            else
                break;
        }
    }
    ptr = head;
    Song *prev = NULL;
    while (ptr != NULL)
    {
        if (a_substring_of_b(query, ptr->p[TITLE])
                && (ct == 1 || id == ptr->id))
        {
            *prevAddr = prev;
            return ptr;
        }
        prev = ptr;
        ptr = ptr->next;
    }
    printf("ID not found.\n\n");
    return NULL;
}

/*
 * Print node contents. If itemNo is not 0, print item number. If is_printid
 * is true, print id.
 */
void print_node(Song *s, int itemNo, int is_printid)
{
    if (itemNo)
        printf("ITEM NO. %d\n", itemNo);
    char *properties[] = {
        "Title:    ",
        "Artist:   ",
        "Composer: ",
        "Album:    ",
        "Remarks:  "
    };
    char *genres[] = {"Art", "Pop", "Traditional"};
    if (is_printid)
        printf("ID:       %08x\n", s->id);
    int i;
    for (i = 0; i < NUMP; i++)
        printf("%s%s\n", properties[i], s->p[i]);
    printf("Rating:   %.2lf/5\n", s->rating);
    printf("Genre:    %s\n", genres[s->genre]);
    printf("--------------------\n");
}

/*
 * Free a node's contents, then the node itself.
 */
void free_node(Song *s)
{
    int i;
    for (i = 0; i < NUMP; i++)
    {
        free(s->p[i]);
    }
    free(s);
}

/*
 * Free all nodes in a list.
 */
void free_list(Song *head)
{
    Song *ptr = head;
    while (ptr != NULL)
    {
        head = ptr;
        ptr = ptr->next;
        free_node(head);
    }
}

Song *find_mid(Song *head)
{
    if (head == NULL) return NULL;
    Song *mid = head;
    Song *ptr = head;
    while (ptr->next != NULL)
    {
        ptr = ptr->next;
        if (ptr->next != NULL)
            ptr = ptr->next;
        else
            return mid;
        mid = mid->next;
    }
    return mid;
}


void sort_list(Song **headAddr, int choice)
{
    Song *head = *headAddr;
    if (head == NULL)
        return;
    if (head->next == NULL)
        return;
    Song *a = head;
    Song *b = find_mid(head);

    Song *ptr = b;
    b = b->next;
    ptr->next = NULL;

    sort_list(&a, choice);
    sort_list(&b, choice);

    int condition = 0;
    if (choice == 1)
        condition = a->rating <= b->rating;
    else if (choice == 2)
        condition = songcmp(a, b) <= 0;
    if (condition)
    {
        head = a;
        a = a->next;
    }
    else
    {
        head = b;
        b = b->next;
    }
    ptr = head;
    while (a != NULL || b != NULL)
    {

        if (a == NULL)
        {
            ptr->next = b;
            b = b->next;
        }
        else if (b == NULL)
        {
            ptr->next = a;
            a = a->next;
        }
        else
        {
            int condition = 0;
            if (choice == 1)
                condition = a->rating <= b->rating;
            else if (choice == 2)
                condition = songcmp(a, b) <= 0;

            if (condition)
            {
                ptr->next = a;
                a = a->next;
            }
            else
            {
                ptr->next = b;
                b = b->next;
            }
        }
        ptr = ptr->next;
    }
    ptr->next = NULL;
    *headAddr = head;
}

/********************
 * Helper functions *
 ********************/
/*
 * Get input string up until newline. and store it dynamically. Must free
 * output to avoid memory leak. Return input string.
 */
char *f_get_string(FILE *stream)
{
    char *str = NULL;
    char ch;
    int n = 0;
    int max = 0;
    while ((ch = fgetc(stream)) != EOF && ch != '\n')
    {
        if (n + 1 > max)
        {
            if (max == 0)
                max = 32;
            else
                max *= 2;
            str = realloc(str, max);
        }
        str[n] = ch;
        n++;
    }
    char *newStr = malloc(n + 1);
    int i;
    for (i = 0; i < n; i++)
    {
        newStr[i] = str[i];
    }
    newStr[i] = '\0';

    free(str);
    return newStr;
}

/*
 * Get input integer. Return input integer if valid input, otherwise return
 * INT_MAX.
 */
int f_get_int(FILE *stream)
{
    char *str = f_get_string(stream);
    int n;
    char ch;
    int isvalid = sscanf(str, " %d %c", &n, &ch);

    if (isvalid != 1) // invalid input
    {
        free(str);
        return INT_MAX;
    }
    free(str);
    return n;
}

/*
 * Get input double. Return input double if valid input, otherwise return
 * DBL_MAX.
 */
double f_get_double(FILE *stream)
{
    char *str = f_get_string(stream);
    double n;
    char ch;
    int isvalid = sscanf(str, " %lf %c", &n, &ch);

    if (isvalid != 1) // invalid input
    {
        free(str);
        return DBL_MAX;
    }
    free(str);
    return n;
}

/*
 * Get input hex integer. Return input integer if valid input, otherwise
 * return INT_MAX.
 */
int f_get_hex(FILE *stream)
{
    char *str = f_get_string(stream);
    int n;
    char ch;
    int isvalid = sscanf(str, " %08x %c", &n, &ch);

    if (isvalid != 1 || strlen(str) != 8) // invalid input
    {
        free(str);
        return INT_MAX;
    }
    free(str);
    return n;
}

/*
 * Use rand() to make the ID random. If new number is not unique, try again.
 */
void gen_id(Song *head, Song *s)
{
    int isUnique;
    do
    {
        isUnique = 1;
        s->id = rand();
        Song *ptr = head;
        while (ptr != NULL)
        {
            if (ptr != s && ptr->id == s->id)
            {
                isUnique = 0;
                break;
            }
            ptr = ptr->next;
        }
    } while (!isUnique);
}

/*
 * Remove leading and trailing spaces.
 */
void remove_edge_spaces(char *s)
{
    int n;
    int spacepos = 0;
    for (n = 0; s[n] != '\0'; n++)
    {
        if (!isspace(s[n]) && spacepos == -1)
        {
            spacepos = n;
        }
        if (spacepos != -1)
        {
            s[n - spacepos] = s[n];
        }
    }
    n = n - spacepos;
    s[n] = '\0';
    for (n--; n >= 0; n--)
    {
        if (!isspace(s[n]))
        {
            s[n + 1] = '\0';
            break;
        }
    }
}

/*
 * Compare two songs based on title and artist. Return <0 if s1 comes before
 * s2, >0 if s1 comes after s2, and 0 if s1 and s2 have the same title & artist
 */
int songcmp(Song *s1, Song *s2)
{
    int titlecmp = strcasecmp(s1->p[TITLE], s2->p[TITLE]);
    if (titlecmp)
        return titlecmp;
    else
        return strcasecmp(s1->p[ARTIST], s2->p[ARTIST]);
}

/*
 * Return 1 if a is substring of b. Otherwise, return 0.
 */
int a_substring_of_b(const char *a, const char *b)
{
    int i, j, k;
    for (i = 0;; i++)
    {
        for (j = 0, k = i;; j++, k++)
        {
            if (a[j] == '\0')
                return 1;
            if (b[k] == '\0')
                return 0;
            if (tolower(a[j]) != tolower(b[k]))
                break;
        }
    }
}
/*
 * The name says it all. Wait for user to press enter.
 */
void wait_enter(void)
{
    free(f_get_string(stdin));
}
